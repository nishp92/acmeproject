package acmelogin;


import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;


public class Acmepage{
	
	public ChromeDriver driver;
	
	
	
	@Given("Open the browser")
	public void OpenChromedriver() {
	
	 driver =new ChromeDriver();
	
	}
	
	@And("maximize the browser")
	public void maximize_browser() {
		driver.manage().window().maximize();
	    
	}

	@And("set the timeout")
	public void settimeout() {
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);  
	    }

	@And("enter the URL")
	public void enter_URL() {
		driver.get("https://acme-test.uipath.com/account/login");
	   
	}

	@And("enterUsername")
	public void enter_username() {
		driver.findElementById("email").sendKeys("nishp23@gmail.com");
	    
	}

	@And("Enterpassword")
	public void enter_password() {
		driver.findElementById("password").sendKeys("Testing123");
	    
	}

	@When("Click login")
	public void click_login_button() {
		driver.findElementById("buttonLogin").click();
	    
	}

	@Then("verify login is successfull")
	public void verify_login_is_successfully() {
	    System.out.println("Login success");
	    
	}
	
	@Given("Open Dashboard")
	public void open_Dashboard() throws InterruptedException {
		Thread.sleep(2000);
		
	}

	@When("Click Vendor button")
	public void click_vendorbutton() {
	  	Actions a = new Actions(driver);
        WebElement w1=driver.findElement(By.xpath("(//button[@type='button'])[6]"));
        a.moveToElement(w1).perform();
        
        
	}
	@When("Click Search vendor button")
	public void click_search_vendorbutton() {
		driver.findElementByPartialLinkText("Search for Vendor").click();
		
	}
	
	@And("EnterTaxID")
	public void enter_TaxID() {
		 driver.findElementById("vendorTaxID").sendKeys("IT231232");
	    
	}
	@When("Click Search button")
	public void click_Searchbutton() {
		driver.findElementById("buttonSearch").click();
	    
	}
	@Then("verify Vendor name is successful")
	public void verify_Vendorname() {
		String text = driver.findElementByXPath("/html/body/div/div[2]/div/table/tbody/tr[2]/td[1]").getText();
        System.out.println(text);
	}
	
}
