package acmeproject;



import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;



public class Acme1 {
	
		public static void main(String[] args) {
			
			ChromeDriver driver =new ChromeDriver();
			driver.get("https://acme-test.uipath.com/account/login");
			driver.manage().window().maximize();
			driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
			
			
			//passing the parameter values
			driver.findElementById("email").sendKeys("nishp23@gmail.com");
			driver.findElementById("password").sendKeys("Testing123");
			driver.findElementById("buttonLogin").click();
			
			//move element to vendor using actions class
			Actions a = new Actions(driver);
	        WebElement w1=driver.findElement(By.xpath("(//button[@type='button'])[6]"));
	        a.moveToElement(w1).perform();
	        driver.findElementByPartialLinkText("Search for Vendor").click();
	        driver.findElementById("vendorTaxID").sendKeys("IT231232");
	        driver.findElementById("buttonSearch").click();
	        //printing the vendor value
	        String text = driver.findElementByXPath("/html/body/div/div[2]/div/table/tbody/tr[2]/td[1]").getText();
	        System.out.println(text);
		}
			
		}
		
        
               
	
