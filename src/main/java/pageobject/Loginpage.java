package pageobject;

import org.openqa.selenium.support.PageFactory;

import framework.base.Projectmethods;

public class Loginpage extends Projectmethods{
	
	public Loginpage() {
		//apply PageFactory
		PageFactory.initElements(driver, this); 
	}
	
	public Loginpage enterUsername() {
		driver.findElementById("email").sendKeys("nishp23@gmail.com");
		return this;
	}	
	public Loginpage enterPassword() {
	driver.findElementById("password").sendKeys("Testing123");
	return this;	
	}
	public Dashboardpage clickloginbutton() {
		driver.findElementById("buttonLogin").click();
		return new Dashboardpage();	
		}
	
}
	
	
