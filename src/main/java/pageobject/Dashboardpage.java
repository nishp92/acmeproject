package pageobject;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.PageFactory;

import framework.base.Projectmethods;

public class Dashboardpage extends Projectmethods {
	
	public Dashboardpage() {
		//apply PageFactory
		PageFactory.initElements(driver, this); 
	}
	
	public Vendorpage actionperform() {
		Actions a = new Actions(driver);
        WebElement w1=driver.findElement(By.xpath("(//button[@type='button'])[6]"));
        a.moveToElement(w1).perform();
        driver.findElementByPartialLinkText("Search for Vendor").click();
		return new Vendorpage();
			
	}
	

	
	

}
