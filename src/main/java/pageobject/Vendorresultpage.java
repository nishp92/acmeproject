package pageobject;

import org.openqa.selenium.support.PageFactory;

import framework.base.Projectmethods;

public class Vendorresultpage extends Projectmethods {
	
	public Vendorresultpage() {
	       PageFactory.initElements(driver, this);
		}

	public Vendorresultpage vendorname() {
		String text = driver.findElementByXPath("/html/body/div/div[2]/div/table/tbody/tr[2]/td[1]").getText();
        System.out.println(text);
		return this;
		
	}
	
}
