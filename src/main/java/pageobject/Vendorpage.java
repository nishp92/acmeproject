package pageobject;


import org.openqa.selenium.support.PageFactory;

import framework.base.Projectmethods;

public class Vendorpage extends Projectmethods {
	
	public Vendorpage() {
	       PageFactory.initElements(driver, this);
		}
	
	public Vendorresultpage enterform() {
		driver.findElementById("vendorTaxID").sendKeys("IT231232");
        driver.findElementById("buttonSearch").click();
		return new Vendorresultpage();
			
	}

}
